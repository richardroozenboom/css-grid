# IMAGE GRID

## A. Using floats

    The starting point of this exercise is a image grid using floats. Each image is 20% of the total width and they are all square. so that looks nice, job done! Hmm maybe not...

    1. Add a class .span-2 and set the image width to 40%.
       (keep in mind the gap between the images)
    2. Let's add some media queries to make it responsive.
       - default: 100% width
       - 768px: 33.333% width (.span-2: 66.666% width)
       - 960px: 25% width (.span-2: 50% width)
       - 1200px: 20% width (.span-2: 40% width)

## B. Switch to css grid

    As you could see in the previous exercise it is possible to create image grids with floats and media queries but it's a bit of a hassle. So let's switch to css grid and find out how this can help us.

    1. set the container to display grid and the grid gap to .5rem
    2. create a column template which fits as many images as
       possible with a minimum of 12.5rem and a max of 1fr.
    3. add a class .span-2, which spans 2 columns and 2 rows.
       - add media query to only span when the min-width is 768px;
