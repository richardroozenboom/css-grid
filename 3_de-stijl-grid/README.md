# DE STIJL

    In this exercise we will use css grid to recreate a famous artwork by Piet Mondriaan [1872 - 1944] : compositie met groot rood vlak, geel, zwart, grijs en blauw uit 1921. (https://www.gemeentemuseum.nl/nl/collectie/compositie-met-groot-rood-vlak-geel-zwart-grijs-en-blauw)

    Mondriaan was part of a group called 'De Stijl'. Some of the base principles of this art movement are:
    - Abstract art
    - Minimalistic
    - Primary colors: red, yellow and blue

## A. Creating an artwork

    1. Create a grid by modifing the unordered list.
       - set the template to 8 equal sized columns
       - set the template to 8 equal sized rows
    2. Overwrite the class .block, which is used for all the blocks.
       - set margin to zero
       - set width to auto
    3. Modify the container to make sure it is always a square.
        - extend the section element with a pseudo before class
            - set display to block
            - set padding-top to 100%
        - position the main element as absolute and set all sides to 0
        - set the width and height of the unordered list to 100%
        - set the height of the .block class to auto
    4. Hide the outside borders of the artwork by adding extending the unordered list with a pseudo before class. Size it full width and height. Add a border of 4px (.25rem) with the background color.

## B. Make it responsive

    1. We've to revert some of the changes from the previous assignment
        - remove the before pseudo class from the section element
        - remove the absolute position of the main element
        - remove the width and height from the unordered list.
    2. Next modify some of the grid container attributes (unordered list)
        - create a column template which auto-fit the content with a minimum size of the css variable --block-size and a maximum size of 1 fractional unit (fr)
        - set auto rows to the css variable --block-size
        - add an auto flow attribute and set it to dense
        - to finalize set the background to the css var --color-black

## C. Play with it

    1. Change the css variable --block-size to a different value and checkout the effect.
    2. Add more block elements to the index.html with different sizes and checkout how the artwork respond.
    3. Checkout the worldmap.html in the a_examples folder to see what can be done with the same classes but a slighly different grid.

# THANKS

You now mastered the css grid!.
