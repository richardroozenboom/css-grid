# RESPONSIVE GRID

## A. Create a basic grid

    1. Set the container to display grid and the grid gap to .5rem
    2. Create 3 columns (240px 1fr 240px) and 4 rows (80px 2fr 1fr 120px)
    3. Set the header and footer to span 3 columns and make the nav and content section span 2 rows

## B. Let's make the grid responsive:

    1. Display all sections in order on mobile:

        | header  |
        | nav     |
        | content |
        | sidebar |
        | ad      |
        | footer  |

    2. Show for the 768px breakpoint:

        | header  | header  |
        | nav     | nav     |
        | content | sidebar |
        | content | ad      |
        | footer  | footer  |

    3. Show from 960px and up:

        | header | header  | header  |
        | nav    | content | sidebar |
        | nav    | content | ad      |
        | footer | footer  | footer  |

## C. Let's use grid areas to simplify things

    1. Modify all three breakpoints to use grid areas
    2. Change the 768px breakpoint to the following layout:

        | header  | header  |
        | nav     | nav     |
        | sidebar | content |
        | ad      | footer  |
